package com.nulp.project.service;

import com.nulp.project.entity.Admin;
import com.nulp.project.repository.AdminRepository;
import com.nulp.project.service.impl.AdminServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AdminServiceTest {

    @Mock
    private AdminRepository adminRepository;
    @Mock
    private AdminService adminService;
    private Admin admin;

    private static final String TEST_PASSWORD = "TestPassword";
    private static final String TEST_LOGIN = "New Login";

    @Before
    public void setUp() {
        admin = new Admin();
        admin.setName(TEST_LOGIN);
        admin.setPassword(TEST_PASSWORD);
        adminRepository = Mockito.mock(AdminRepository.class);
        adminService = Mockito.mock(AdminService.class);
        when(adminRepository.findAdminByNameAndPassword(anyString(),anyString())).thenReturn(admin);
        when(adminService.signInAdmin(TEST_LOGIN,TEST_PASSWORD)).thenReturn(true);
    }

    @Test
    public void adminSignInTest(){
        assertTrue(adminService.signInAdmin(TEST_LOGIN,TEST_PASSWORD));
    }
}
