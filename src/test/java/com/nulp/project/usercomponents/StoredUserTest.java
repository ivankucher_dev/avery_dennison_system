package com.nulp.project.usercomponents;

import com.nulp.project.entity.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StoredUserTest {

  private StoredUser storedUser;
  private User user;
  private static final String TEST_PASSWORD = "TestPassword";
  private static final String TEST_LOGIN = "New Login";

  @Before
  public void setUp() {
    user = new User();
    user.setLogin(TEST_LOGIN);
    user.setPassword(TEST_PASSWORD);
    storedUser = new StoredUser();
    storedUser.setUser(user);
    storedUser.setPassword(TEST_PASSWORD);
  }

  @Test
  public void testStoredUser() {
    assertEquals(user.getLogin(), storedUser.getUser().getLogin());
    assertEquals(user.getPassword(), storedUser.getUser().getPassword());
  }

  @Test
    public void testStoredData(){
      assertEquals(TEST_PASSWORD,storedUser.getUser().getPassword());
  }
}
