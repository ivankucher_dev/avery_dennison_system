package com.nulp.project.util;

import com.nulp.project.entity.Address;
import com.nulp.project.entity.Customer;
import com.nulp.project.entity.User;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CustomerFactoryTest {

    private User user;
    private static final String TEST_NUMBER = "0986453533";
    private static final Address TEST_ADDRESS = new Address();
    private static final String TEST_NAME = "testName";
    private static final String TEST_SURNAME = "testSurname";

    @Before
    public void setUp() {
        user = new User();
        user.setName(TEST_NAME);
        user.setSurname(TEST_SURNAME);
        user.setPhoneNumber(TEST_NUMBER);
        user.setAddress(TEST_ADDRESS);
    }

    @Test
    public void testFactory(){
        Customer customer = CustomerFactory.createCustomer(user);
        assertEquals(customer.getName(),user.getName());
        assertEquals(customer.getAddress_id(),user.getAddress());
        assertEquals(customer.getSurname(),user.getSurname());

    }
}
