package com.nulp.project.dto;

public class ShipmentDto {

  private String name;

  private int finalPrice;

  private String deliveryCompanyName;

  private String transportCode;

    public ShipmentDto(String name, int finalPrice, String deliveryCompanyName, String transportCode) {
        this.name = name;
        this.finalPrice = finalPrice;
        this.deliveryCompanyName = deliveryCompanyName;
        this.transportCode = transportCode;
    }

    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getFinalPrice() {
    return finalPrice;
  }

  public void setFinalPrice(int finalPrice) {
    this.finalPrice = finalPrice;
  }

  public String getDeliveryCompanyName() {
    return deliveryCompanyName;
  }

  public void setDeliveryCompanyName(String deliveryCompanyName) {
    this.deliveryCompanyName = deliveryCompanyName;
  }

  public String getTransportCode() {
    return transportCode;
  }

  public void setTransportCode(String transportCode) {
    this.transportCode = transportCode;
  }
}
