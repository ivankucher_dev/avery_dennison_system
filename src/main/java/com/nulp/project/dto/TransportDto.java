package com.nulp.project.dto;

import com.nulp.project.entity.Transport;

public class TransportDto {

  private Transport transport;
  private int typeId;

    public TransportDto(Transport transport, int typeId) {
        this.transport = transport;
        this.typeId = typeId;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId =typeId;
    }
}
