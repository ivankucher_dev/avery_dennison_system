package com.nulp.project.dto;

import com.nulp.project.entity.Shipment;
import com.nulp.project.entity.Transport;

import java.util.List;

public class PdfReportDto {

    private List<Shipment> shipments;
    private List<Transport> transports;

    public PdfReportDto(List<Shipment> shipments, List<Transport> transports) {
        this.shipments = shipments;
        this.transports = transports;
    }

    public List<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(List<Shipment> shipments) {
        this.shipments = shipments;
    }

    public List<Transport> getTransports() {
        return transports;
    }

    public void setTransports(List<Transport> transports) {
        this.transports = transports;
    }
}
