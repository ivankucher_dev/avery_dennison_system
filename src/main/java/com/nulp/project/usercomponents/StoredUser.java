package com.nulp.project.usercomponents;

import com.nulp.project.entity.User;
import org.springframework.stereotype.Component;

@Component
public class StoredUser extends User{
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
