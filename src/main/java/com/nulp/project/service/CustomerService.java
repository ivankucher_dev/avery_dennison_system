package com.nulp.project.service;

import com.nulp.project.entity.Customer;

import java.util.List;

public interface CustomerService {
    Customer addCustomer(Customer customer);
    void delete(int id);
    Customer getByName(String name);
    Customer update(Customer customer);
    List<Customer> getAll();
    void save(Customer customer);
}
