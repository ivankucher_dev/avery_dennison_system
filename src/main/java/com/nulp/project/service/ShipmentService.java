package com.nulp.project.service;

import com.nulp.project.entity.Shipment;

import java.util.List;
public interface ShipmentService {
    Shipment addShipment(Shipment shipment);
    void delete(int id);
    Shipment getByName(String name);
    Shipment update(Shipment shipment);
    List<Shipment> getAll();
}
