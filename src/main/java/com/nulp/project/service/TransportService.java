package com.nulp.project.service;

import com.nulp.project.entity.Transport;

import java.util.List;

public interface TransportService {
    Transport getTransportByCode(String transportCode);
    List<Transport> getAll();
}
