package com.nulp.project.service;

import com.nulp.project.entity.DeliveryCompany;

import java.util.List;

public interface DeliveryService {
  void delete(int id);

  DeliveryCompany getByName(String name);

  List<DeliveryCompany> getAll();
}
