package com.nulp.project.service.impl;

import com.nulp.project.entity.Customer;
import com.nulp.project.repository.CustomerRepository;
import com.nulp.project.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository repository;

    @Override
    public Customer addCustomer(Customer customer) {
        return repository.saveAndFlush(customer);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    @Override
    public Customer getByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Customer update(Customer customer) {
        return repository.saveAndFlush(customer);
    }

    @Override
    public List<Customer> getAll() {
        return repository.findAll();
    }

    @Override
    public void save(Customer customer) {
        repository.save(customer);
    }
}
