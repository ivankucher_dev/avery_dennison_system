package com.nulp.project.service.impl;

import com.nulp.project.entity.DeliveryCompany;
import com.nulp.project.repository.DeliveryCompanyRepository;
import com.nulp.project.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeliveryCompanyServiceImpl implements DeliveryService {

  @Autowired DeliveryCompanyRepository repository;

  @Override
  public void delete(int id) {
      repository.deleteById(id);
  }

  @Override
  public DeliveryCompany getByName(String name) {
    DeliveryCompany company = repository.findDeliveryCompanyByName(name);
    return repository.findDeliveryCompanyByName(name);
  }

  @Override
  public List<DeliveryCompany> getAll() {
    return repository.findAll();
  }
}
