package com.nulp.project.service.impl;

import com.nulp.project.entity.Admin;
import com.nulp.project.repository.AdminRepository;
import com.nulp.project.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {

  @Autowired AdminRepository adminRepository;

  @Override
  public boolean signInAdmin(String name, String password) {
    Admin admin = adminRepository.findAdminByNameAndPassword(name, password);
    return admin != null;
  }
}
