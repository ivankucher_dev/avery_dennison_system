package com.nulp.project.service.impl;

import com.nulp.project.entity.Customer;
import com.nulp.project.entity.User;
import com.nulp.project.repository.CustomerRepository;
import com.nulp.project.repository.UserRepository;
import com.nulp.project.service.UserService;
import com.nulp.project.util.CustomerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
  @Autowired private UserRepository userRepository;
  @Autowired private CustomerRepository customerRepository;

  @Override
  public boolean findUserByLoginAndPassword(String login, String password) {
    User user = userRepository.findUserByLoginAndPassword(login, password);
    return user != null;
  }

  @Override
  public User findUserBySignInMethod(String login, String password) {
    return userRepository.findUserByLoginAndPassword(login,password);
  }

  @Override
  public User registerCustomer(String login,String password) {
    User user = userRepository.findUserByLoginAndPassword(login,password);
    Customer customer = CustomerFactory.createCustomer(user);
    customerRepository.save(customer);
    user.setCustomer(customer);
    userRepository.save(user);
    return user;
  }
}
