package com.nulp.project.service.impl;

import com.nulp.project.entity.Shipment;
import com.nulp.project.repository.ShipmentRepository;
import com.nulp.project.service.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipmentServiceImpl implements ShipmentService {

    @Autowired
    private ShipmentRepository shipmentRepository;

    @Override
    public Shipment addShipment(Shipment shipment) {
        Shipment savedShipment = shipmentRepository.saveAndFlush(shipment);
        return savedShipment;
    }

    @Override
    public void delete(int id) {
        shipmentRepository.deleteById(id);
    }

    @Override
    public Shipment getByName(String name) {
        return shipmentRepository.findByName(name);
    }

    @Override
    public Shipment update(Shipment shipment) {
        return shipmentRepository.saveAndFlush(shipment);
    }

    @Override
    public List<Shipment> getAll() {
        return shipmentRepository.findAll();
    }
}
