package com.nulp.project.service.impl;

import com.nulp.project.entity.Transport;
import com.nulp.project.repository.TransportRepository;
import com.nulp.project.service.TransportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransportServiceImpl implements TransportService {
    @Autowired
    TransportRepository transportRepository;
    @Override
    public Transport getTransportByCode(String transportCode) {
        return transportRepository.getTransportByTransportCode(transportCode);
    }

    @Override
    public List<Transport> getAll() {
        return transportRepository.findAll();
    }
}
