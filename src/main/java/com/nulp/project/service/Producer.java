package com.nulp.project.service;

import com.nulp.project.config.KafkaProperties;
import com.nulp.project.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Service
@Slf4j
public class Producer {

  private final KafkaProperties kafkaProperties;

  private final KafkaTemplate<String, User> kafkaTemplate;

    public Producer(KafkaProperties kafkaProperties, KafkaTemplate<String, User> kafkaTemplate) {
        this.kafkaProperties = kafkaProperties;
        this.kafkaTemplate = kafkaTemplate;
    }

    public HttpStatus sendMessage(User user) {
    ListenableFuture<SendResult<String, User>> future = kafkaTemplate.send(kafkaProperties.getKucherTopic(), user);
        final boolean[] success = new boolean[1];
        success[0] = true;

    future.addCallback(
        new ListenableFutureCallback<SendResult<String, User>>() {

          @Override
          public void onSuccess(SendResult<String, User> result) {
            log.info("Message sent -----> {}", user.toString());
            success[0] = true;
          }

          @Override
          public void onFailure(Throwable ex) {
            log.error("unable to send message = {} , with exception : {}", user.toString(), ex);
            success[0] = false;
          }
        });

   return success[0] ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
  }

}
