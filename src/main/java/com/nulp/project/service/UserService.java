package com.nulp.project.service;

import com.nulp.project.entity.User;

public interface UserService {
    boolean findUserByLoginAndPassword(String login,String password);
    User findUserBySignInMethod(String login,String password);
    User registerCustomer(String login,String password);
}
