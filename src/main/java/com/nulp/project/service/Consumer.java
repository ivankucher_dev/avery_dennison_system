package com.nulp.project.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nulp.project.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class Consumer {

  @Autowired ObjectMapper objectMapper;

  @KafkaListener(topics = "kucher-test-topic", groupId = "topic_group")
  public void consume(String userJson) throws IOException {
    User user = objectMapper.readValue(userJson, User.class);
    log.info("Consumed message <-------- {}", user.toString());
  }
}
