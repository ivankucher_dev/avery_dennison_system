package com.nulp.project.service;

public interface AdminService {
    boolean signInAdmin(String name, String password);
}
