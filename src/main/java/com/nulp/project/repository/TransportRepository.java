package com.nulp.project.repository;

import com.nulp.project.entity.Transport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransportRepository extends JpaRepository<Transport, Integer> {
    Transport getTransportByTransportCode(String transportCode);
}
