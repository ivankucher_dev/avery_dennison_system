package com.nulp.project.repository;

import com.nulp.project.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserByLoginAndPassword(String login,String password);
}
