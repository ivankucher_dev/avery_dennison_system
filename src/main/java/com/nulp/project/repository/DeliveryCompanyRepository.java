package com.nulp.project.repository;

import com.nulp.project.entity.DeliveryCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryCompanyRepository extends JpaRepository<DeliveryCompany, Integer> {
    DeliveryCompany findDeliveryCompanyByName(String name);
}
