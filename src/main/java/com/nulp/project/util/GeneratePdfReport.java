package com.nulp.project.util;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nulp.project.dto.PdfReportDto;
import com.nulp.project.entity.Shipment;
import com.nulp.project.entity.Transport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.util.List;
import java.util.*;

public class GeneratePdfReport {


    public static ByteArrayInputStream generate(PdfReportDto pdfReportDto,String generatedBy) {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Paragraph p = new Paragraph();
        Date date = new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        int avgPrice=0;
        HashMap<String,Integer> companies = new HashMap<>();

        try {

            p.add("Avery Dennison system report");
            p.add("\n =============================================================");
            p.add("\n Avery Dennison version :      v.1.1.3.0");
            p.add("\n Avery Dennison report time created :    "+ts.toString()+"\n\n");
            p.add("\n\n Shipments report :    \n\n");
            PdfWriter.getInstance(document, out);
            document.open();
            document.add(p);
            PdfPTable table = new PdfPTable(7);
            table.setWidthPercentage(100);

            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Name", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Delivery company", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Final price", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Time created", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Customer", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Status", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Transport", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            for (Shipment shipment : pdfReportDto.getShipments()) {

                avgPrice+= shipment.getFinalPrice();
                if(companies.containsKey(shipment.getDeliveryCompany().getName())){
                    companies.put(shipment.getDeliveryCompany().getName(),companies.get(shipment.getDeliveryCompany().getName())+1);
                }
                else{
                    companies.put(shipment.getDeliveryCompany().getName(),1);
                }
                PdfPCell cell;

                cell = new PdfPCell(new Phrase(shipment.getName()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(shipment.getDeliveryCompany().getName()));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(shipment.getFinalPrice())));
                cell.setPaddingRight(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);


                cell = new PdfPCell(new Phrase(String.valueOf(shipment.getTimeCreated().toString())));
                cell.setPaddingRight(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(shipment.getCustomer().getName()+" "+shipment.getCustomer().getSurname()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingLeft(5);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(shipment.getDeliveryStatus().toString()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingLeft(5);
                table.addCell(cell);


                cell = new PdfPCell(new Phrase(shipment.getTransport().getTransportCode()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingLeft(5);
                table.addCell(cell);
            }

            document.add(table);
            p = new Paragraph();
            p.add("\nAvg final price :           "+avgPrice+" $\n\n");
            p.add("Top 3 delivery companies :  \n");
            companies = sortByValue(companies);
            int i=0;
            for(Map.Entry entry : companies.entrySet()){
                if(i==3){
                    break;
                }
                p.add(entry.getKey()+" with amount of shipments = "+entry.getValue()+"\n");
                i++;
            }

            p.add("\nRegistered transports : \n\n");
            document.add(p);

            PdfPTable transportTable = new PdfPTable(3);
            table.setWidthPercentage(100);

            hcell = new PdfPCell(new Phrase("gps location", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            transportTable.addCell(hcell);

            hcell = new PdfPCell(new Phrase("transport code", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            transportTable.addCell(hcell);

            hcell = new PdfPCell(new Phrase("shipment type", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            transportTable.addCell(hcell);

            for (Transport transport : pdfReportDto.getTransports()){
                PdfPCell cell;

                cell = new PdfPCell(new Phrase(String.valueOf(transport.getGpsLocation())));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                transportTable.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(transport.getTransportCode())));
                cell.setPaddingLeft(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                transportTable.addCell(cell);

                cell = new PdfPCell(new Phrase(transport.getShipmentType().getType()));
                cell.setPaddingRight(5);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                transportTable.addCell(cell);
            }
            document.add(transportTable);
            p=new Paragraph();
            p.add("\n\nGenerated by admin with login :            "+generatedBy);
            document.add(p);
            document.close();

        } catch (DocumentException ex) {
      System.out.println("Error occured! \n"+ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }


    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm) {
        List<Map.Entry<String, Integer> > list = new LinkedList<>(hm.entrySet());

        Collections.sort(list, (o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));

        HashMap<String, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }
}