package com.nulp.project.util;

import com.nulp.project.entity.Customer;
import com.nulp.project.entity.User;

public class CustomerFactory {
    public static Customer createCustomer(User user){
        Customer customer = new Customer();
        customer.setAddress_id(user.getAddress());
        customer.setName(user.getName());
        customer.setSurname(user.getSurname());
        customer.setPhoneNumber(user.getPhoneNumber());
        customer.setZip(generateZip());
        return customer;
    }

    private static String generateZip(){
        return String.valueOf(RandomGenerator.generateInteger(12333,99999));
    }

}
