package com.nulp.project.controller;

import com.nulp.project.dto.PdfReportDto;
import com.nulp.project.entity.*;
import com.nulp.project.enums.DeliveryStatus;
import com.nulp.project.enums.RequestStatus;
import com.nulp.project.repository.*;
import com.nulp.project.service.AdminService;
import com.nulp.project.util.GeneratePdfReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {

  @Autowired private AdminService adminService;
  @Autowired private TransportRepository transportRepository;
  @Autowired private ShipmentTypeRepository shipmentTypeRepository;
  @Autowired private ShipmentRepository shipmentRepository;
  @Autowired private CustomerRepository customerRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private AdminRepository adminRepository;
  private String adminLogin;
  private String adminPassword;

  @GetMapping("")
  public String showMenu() {
    return "signInAdminPanel";
  }

  @PostMapping("/signIn")
  public String loginAdmin(String name, String password, Model model) {
    boolean exist = adminService.signInAdmin(name, password);
    if (!exist) {
      model.addAttribute("error", "Admin doesnt exist! Try again !");
      return "erroradminpage";
    }
    adminLogin = name;
    adminPassword = password;
    return "adminPanel";
  }

  @GetMapping("/editStatus")
  public String getEditStatusPage() {
    return "findShipmentById";
  }

  @GetMapping("/users")
  public String getUsersWorkingPage() {
    return "adminUsers";
  }

  @GetMapping("/users/delete-user-find")
  public String getUsersFindByIdPage() {
    return "findUserById";
  }

  @GetMapping("/users/edit-user-find")
  public String getUsersFindByIdForEditPage() {
    return "findUserByIdForEdit";
  }

  @GetMapping("/users/delete-user")
  public String getUserById(int userId, Model model) {
    Optional<User> user = userRepository.findById(userId);
    if (!user.isPresent()) {
      model.addAttribute("statusMessage", "User doesnt exist! Deleting failed");
      return "adminPanel";
    }
    model.addAttribute("user", user.get());
    return "deleteUserPage";
  }

  @GetMapping("/users/edit-user")
  public String getUserForEditById(int userId, Model model) {
    Optional<User> user = userRepository.findById(userId);
    if (!user.isPresent()) {
      model.addAttribute("statusMessage", "User doesnt exist! Editing failed");
      return "adminPanel";
    }
    model.addAttribute("user", user.get());
    return "editUserPage";
  }

  @PostMapping("/users/delete-user-customer")
  public String editUserDeleteCustomer(int userId, int customerId, Model model) {
    Optional<Customer> customer = customerRepository.findById(customerId);
    if (!customer.isPresent()) {
      model.addAttribute("statusMessage", "Customer doesnt exist! Editing failed");
      return "adminPanel";
    }
    customerRepository.delete(customer.get());
    model.addAttribute(
        "statusMessage",
        "Deleting customer with id - "
            + customerId
            + " from user with id - "
            + userId
            + " successfully processed");
    return "adminPanel";
  }

  @PostMapping("/users/edit-user")
  public String editUser(User user, Model model) {
    User userForEdit = userRepository.findById(user.getId()).get();
    if (userForEdit == null) {
      model.addAttribute("statusMessage", "User doesnt exist! Editing failed");
      return "adminPanel";
    }
    userForEdit.setName(user.getName());
    userForEdit.setSurname(user.getSurname());
    userForEdit.setLogin(user.getLogin());
    userForEdit.setPassword(user.getPassword());
    userForEdit.setPhoneNumber(user.getPhoneNumber());
    userRepository.save(userForEdit);

    String changedFields =
        user.getName()
            + " before was "
            + userForEdit.getName()
            + "</br>"
            + user.getSurname()
            + " before was "
            + userForEdit.getSurname()
            + "</br>"
            + user.getLogin()
            + " before was "
            + userForEdit.getLogin()
            + "</br>"
            + user.getPassword()
            + " before was "
            + userForEdit.getPassword()
            + "</br>"
            + user.getPhoneNumber()
            + " before was "
            + userForEdit.getPhoneNumber();

    model.addAttribute(
        "statusMessage",
        "Editing user with id - "
            + userForEdit.getId()
            + " successfully processed"
            + "</br>Changed fields :</br> "
            + changedFields);
    return "adminPanel";
  }

  @PostMapping("/users/delete-user")
  public String deleteUser(int userId, Model model) {
    Optional<User> user = userRepository.findById(userId);
    if (!user.isPresent()) {
      model.addAttribute("statusMessage", "User doesnt exist! Deleting failed");
      return "adminPanel";
    }
    userRepository.delete(user.get());
    customerRepository.delete(user.get().getCustomer());
    model.addAttribute(
        "statusMessage",
        "User with id - "
            + userId
            + " and his customer profile with id - "
            + user.get().getCustomer().getCustomerId()
            + " deleted successfully");
    return "adminPanel";
  }

  @PostMapping("/editStatus")
  public String findShipmentById(int shipmentId, Model model) {
    Optional<Shipment> shipment = shipmentRepository.findById(shipmentId);
    if (!shipment.isPresent()) {
      model.addAttribute("statusMessage", "Shipment doesnt exist!");
      return "adminPanel";
    }
    model.addAttribute("shipment", shipment.get());
    model.addAttribute("statuses", Arrays.asList(DeliveryStatus.values()));
    return "editStatusPage";
  }

  @PostMapping("/changeStatus")
  public String changeStatusOfShipment(int shipmentId, String deliveryStatus, Model model) {
    Optional<Shipment> shipment = shipmentRepository.findById(shipmentId);
    if (!shipment.isPresent()) {
      model.addAttribute("statusMessage", "Shipment doesnt exist! Delivery status wasn't changed");
      return "adminPanel";
    }

    shipment.get().setDeliveryStatus(DeliveryStatus.valueOf(deliveryStatus));
    shipmentRepository.save(shipment.get());
    model.addAttribute(
        "statusMessage",
        "Status of shipment with id "
            + shipmentId
            + " was successfully changed to "
            + deliveryStatus);
    return "adminPanel";
  }

  @GetMapping("/transport")
  public String getPageForTransport() {
    return "transport";
  }

  @GetMapping("/transport/add-transport")
  public String getPageForAddingTransport(Model model) {
    model.addAttribute("shipmentTypes", shipmentTypeRepository.findAll());
    return "addTransport";
  }

  @PostMapping("/transport/add-transport")
  public String getPageForAddingTransport(Transport transport, int typeId, Model model) {
    ShipmentType shipmentType = shipmentTypeRepository.findById(typeId).get();
    transport.setShipmentType(shipmentType);
    transportRepository.save(transport);
    model.addAttribute(
        "statusMessage",
        "New transport with transport code "
            + transport.getTransportCode()
            + " was successfully saved");
    return "adminPanel";
  }

    @GetMapping("/transport/delete-transport-find-by-id")
    public String getPageFindTransportById() {
        return "findTransportById";
    }

    @GetMapping("/transport/delete-transport")
    public String getPageForDeleteTransport(int transportId, Model model) {
        Optional<Transport> transport = transportRepository.findById(transportId);
        if (!transport.isPresent()) {
            model.addAttribute("statusMessage", "Transport doesnt exist!");
            return "adminPanel";
        }
        model.addAttribute("transport", transport.get());
        return "deleteTransport";
    }

    @PostMapping("/transport/delete-transport")
    public String deleteTransport(int transportId, Model model) {
        Optional<Transport> transport = transportRepository.findById(transportId);
        if (!transport.isPresent()) {
            model.addAttribute("statusMessage", "Transport doesnt exist!");
            return "adminPanel";
        }
        transportRepository.delete(transport.get());
        model.addAttribute("statusMessage", "Transport with transport code - "+transport.get().getTransportCode()+" deleted successfully!");
        return "adminPanel";
    }




  @RequestMapping(value = "/generateReport", method = RequestMethod.GET,
          produces = MediaType.APPLICATION_PDF_VALUE)
  public ResponseEntity<InputStreamResource> pdfReport() {

    PdfReportDto pdfReportDto = new PdfReportDto(shipmentRepository.findAll(),transportRepository.findAll());

    ByteArrayInputStream bis = GeneratePdfReport.generate(pdfReportDto,adminLogin);

    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=AveryDennisonReport.pdf");

    return ResponseEntity
            .ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(new InputStreamResource(bis));
  }


    @GetMapping("/review-requests-for-shipments")
    public String getReviewShipmentsPage(Model model) {
        List<Shipment> shipmentsToReview = shipmentRepository.findAll();
        shipmentsToReview = shipmentsToReview.stream()
                .filter(shipment->shipment.getRequestStatus().equals(RequestStatus.WAITING_FOR_APPROVE))
                .collect(Collectors.toList());
        if (shipmentsToReview.isEmpty()) {
            model.addAttribute("statusMessage", "No shipmentsToReview to review!");
            return "adminPanel";
        }
        model.addAttribute("shipmentsToReview", shipmentsToReview);
        model.addAttribute("adminLogin",adminLogin);
        return "reviewShipments";
    }

  @PostMapping("/proceed-shipment-status")
  public String proceedShipmentStatus(int shipmentId, String requestStatus, Model model) {
    Shipment shipment = shipmentRepository.findById(shipmentId).get();
    shipment.setRequestStatus(RequestStatus.valueOf(requestStatus));
    shipmentRepository.save(shipment);
    model.addAttribute("statusMessage", "Request status of shipment with id -"+shipment.getShipmentId()+" was changed to "+requestStatus);
    return "adminPanel";
  }

}
