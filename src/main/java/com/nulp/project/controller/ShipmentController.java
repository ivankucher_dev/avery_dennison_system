package com.nulp.project.controller;

import com.nulp.project.dto.ShipmentDto;
import com.nulp.project.entity.DeliveryCompany;
import com.nulp.project.entity.Shipment;
import com.nulp.project.entity.Transport;
import com.nulp.project.enums.DeliveryStatus;
import com.nulp.project.enums.RequestStatus;
import com.nulp.project.repository.ShipmentRepository;
import com.nulp.project.service.CustomerService;
import com.nulp.project.service.DeliveryService;
import com.nulp.project.service.ShipmentService;
import com.nulp.project.service.TransportService;
import com.nulp.project.usercomponents.StoredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/shipments")
@Controller
public class ShipmentController {

  @Autowired ShipmentService shipmentService;
  @Autowired StoredUser storedUser;
  @Autowired CustomerService customerService;
  @Autowired DeliveryService deliveryService;
  @Autowired TransportService transportService;
  @Autowired ShipmentRepository repository;

  @GetMapping("/track")
  public String showMenu(Model model) {
    List<Shipment> shipments = shipmentService.getAll();
    System.out.println(shipments.size());
    model.addAttribute("shipments", shipments);
    return "track-shipment";
  }

  @GetMapping("/addShipment")
  public String addShipmentPage(Model model) {
    model.addAttribute(
        "deliveryCompanies",
        deliveryService.getAll().stream()
            .map(DeliveryCompany::getName)
            .collect(Collectors.toList()));
    model.addAttribute(
        "transportCodes",
        transportService.getAll().stream()
            .map(Transport::getTransportCode)
            .collect(Collectors.toList()));
    return "addShipment";
  }

  @PostMapping("/addShipment")
  public String addShipment(ShipmentDto shipmentDto, Model model) {
    Shipment shipment = new Shipment();
    shipment.setName(shipmentDto.getName());
    shipment.setDeliveryCompany(deliveryService.getByName(shipmentDto.getDeliveryCompanyName()));
    shipment.setDeliveryStatus(DeliveryStatus.DELIVERING);
    shipment.setFinalPrice(shipmentDto.getFinalPrice());
    Date date = new Date();
    long time = date.getTime();
    Timestamp ts = new Timestamp(time);
    shipment.setTimeCreated(ts);
    shipment.setTransport(transportService.getTransportByCode(shipmentDto.getTransportCode()));
    shipment.setRequestStatus(RequestStatus.WAITING_FOR_APPROVE);
    storedUser.getUser().getCustomer().addShipment(shipment);
    customerService.save(storedUser.getUser().getCustomer());
    shipment.setCustomer(storedUser.getUser().getCustomer());
    repository.save(shipment);
    model.addAttribute("message", "Shipment saved successfully");
    return "successfullPage";
  }
}
