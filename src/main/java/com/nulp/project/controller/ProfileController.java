package com.nulp.project.controller;

import com.nulp.project.entity.Shipment;
import com.nulp.project.entity.User;
import com.nulp.project.enums.RequestStatus;
import com.nulp.project.repository.ShipmentRepository;
import com.nulp.project.repository.UserRepository;
import com.nulp.project.service.UserService;
import com.nulp.project.usercomponents.StoredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/profile")
public class ProfileController {

  @Autowired UserService userService;
  @Autowired StoredUser storedUser;
  @Autowired UserRepository userRepository;
  @Autowired ShipmentRepository shipmentRepository;

  @PostMapping("/login")
  public String loginUser(String login, String password, Model model) {
    boolean exist = userService.findUserByLoginAndPassword(login, password);
    if (!exist) {
      model.addAttribute("error", "User not found!");
      return "errorpage";
    }
    User user = userService.findUserBySignInMethod(login, password);
    model.addAttribute("user", user);
    storedUser.setUser(user);
    return "profile";
  }

  @GetMapping
  public String userProfile(Model model) {
    if (storedUser.getUser() == null) {
      model.addAttribute("error", "You are not logged in yet");
      return "redirect:/index";
    }
    model.addAttribute("user", storedUser.getUser());
    return "profile";
  }

  @PostMapping("/registerCustomer")
  public String registerCustomer(String login, String password, Model model) {
    User user = userService.registerCustomer(login, password);
    storedUser.setUser(user);
    model.addAttribute("user", storedUser.getUser());
    model.addAttribute("statusMessage", "You have successfully registered customer with zip - "+user.getCustomer().getZip());
    return "redirect:/profile";
  }

  @GetMapping("/customerOverview")
  public String overviewCustomer(String login, String password, Model model) {
    User user = userService.findUserBySignInMethod(login, password);
    List<Shipment> shipments = shipmentRepository.findAll();
    List<Shipment> shipmentsApproved = shipments.stream()
            .filter(shipment -> shipment.getRequestStatus().equals(RequestStatus.APPROVED))
            .collect(Collectors.toList());
    List<Shipment> shipmentsRejected = shipments.stream()
            .filter(shipment -> shipment.getRequestStatus().equals(RequestStatus.REJECTED))
            .collect(Collectors.toList());
    List<Shipment> shipmentsWaitingForApprove = shipments.stream()
            .filter(shipment -> shipment.getRequestStatus().equals(RequestStatus.WAITING_FOR_APPROVE))
            .collect(Collectors.toList());
    model.addAttribute("customer", user.getCustomer());
    model.addAttribute("user", storedUser.getUser());
    model.addAttribute("shipmentsWaitingForApprove",shipmentsWaitingForApprove);
    model.addAttribute("shipmentsApproved",shipmentsApproved);
    model.addAttribute("shipmentsRejected",shipmentsRejected);
    model.addAttribute("shipments", storedUser.getUser().getCustomer().getShipments());
    return "customerOverview";
  }

  @GetMapping("/userOverview")
  public String overviewUser(Model model) {
    model.addAttribute("user", storedUser.getUser());
    model.addAttribute("customer", storedUser.getUser().getCustomer());
    return "userOverview";
  }
}
