package com.nulp.project.controller;

import com.nulp.project.entity.User;
import com.nulp.project.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/kafka")
@Controller
public class PublishController {

  @Autowired private Producer producer;

  @PostMapping("/send-def-message")
  public ResponseEntity sendToKafkaDefaultMessage(@RequestBody User user) {
    HttpStatus httpStatus = producer.sendMessage(user);

    return new ResponseEntity<>(user, httpStatus);
  }
}
