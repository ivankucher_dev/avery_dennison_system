package com.nulp.project.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class KafkaProperties {

  @Value("${kafka.topic.kucher-test}")
  private String kucherTopic;

}
