package com.nulp.project.entity;

import com.nulp.project.enums.DeliveryStatus;
import com.nulp.project.enums.RequestStatus;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Shipment {
  @Id
  @org.springframework.data.annotation.Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "shipment_id")
  private int shipmentId;

  private String name;

  @Column(name = "final_price")
  private int finalPrice;

  @Column(name = "time_created")
  private Timestamp timeCreated;

  @Column(name = "delivery_status")
  @Enumerated(EnumType.STRING)
  private DeliveryStatus deliveryStatus;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer")
  private Customer customer;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "company")
  private DeliveryCompany deliveryCompany;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "transport")
  private Transport transport;

  @Column(name = "request_status")
  @Enumerated(EnumType.STRING)
  private RequestStatus requestStatus;

  public Shipment(
      int shipmentId,
      String name,
      int finalPrice,
      Timestamp timeCreated,
      DeliveryStatus deliveryStatus,
      Customer customer,
      DeliveryCompany deliveryCompany,
      Transport transport,
       RequestStatus requestStatus) {
    this.shipmentId = shipmentId;
    this.name = name;
    this.finalPrice = finalPrice;
    this.timeCreated = timeCreated;
    this.deliveryStatus = deliveryStatus;
    this.customer = customer;
    this.deliveryCompany = deliveryCompany;
    this.transport = transport;
    this.requestStatus = requestStatus;
  }



  public Shipment() {}

  public RequestStatus getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(RequestStatus requestStatus) {
    this.requestStatus = requestStatus;
  }

  public int getShipmentId() {
    return shipmentId;
  }


  public void setShipmentId(int shipmentId) {
    this.shipmentId = shipmentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getFinalPrice() {
    return finalPrice;
  }

  public void setFinalPrice(int finalPrice) {
    this.finalPrice = finalPrice;
  }

  public Timestamp getTimeCreated() {
    return timeCreated;
  }

  public void setTimeCreated(Timestamp timeCreated) {
    this.timeCreated = timeCreated;
  }

  public DeliveryStatus getDeliveryStatus() {
    return deliveryStatus;
  }

  public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
    this.deliveryStatus = deliveryStatus;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;

    Shipment shipment = (Shipment) object;

    if (shipmentId != shipment.shipmentId) return false;
    if (finalPrice != shipment.finalPrice) return false;
    if (name != null ? !name.equals(shipment.name) : shipment.name != null) return false;
    if (timeCreated != null
        ? !timeCreated.equals(shipment.timeCreated)
        : shipment.timeCreated != null) return false;
    if (deliveryStatus != null
        ? !deliveryStatus.equals(shipment.deliveryStatus)
        : shipment.deliveryStatus != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = shipmentId;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + finalPrice;
    result = 31 * result + (timeCreated != null ? timeCreated.hashCode() : 0);
    result = 31 * result + (deliveryStatus != null ? deliveryStatus.hashCode() : 0);
    return result;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public DeliveryCompany getDeliveryCompany() {
    return deliveryCompany;
  }

  public void setDeliveryCompany(DeliveryCompany deliveryCompany) {
    this.deliveryCompany = deliveryCompany;
  }

  public Transport getTransport() {
    return transport;
  }

  public void setTransport(Transport transport) {
    this.transport = transport;
  }
}
