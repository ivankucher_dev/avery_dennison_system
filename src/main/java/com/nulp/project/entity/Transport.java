package com.nulp.project.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Transport {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "transport_id")
  private int transportId;

  @Column(name = "gps_location")
  private String gpsLocation;

  @Column(name = "transport_code")
  private String transportCode;

  @OneToMany(mappedBy = "transport", fetch = FetchType.LAZY)
  private Collection<Shipment> shipments;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "shipment_type_id")
  private ShipmentType shipmentType;

  public ShipmentType getShipmentType() {
    return shipmentType;
  }

  public void setShipmentType(ShipmentType shipmentType) {
    this.shipmentType = shipmentType;
  }

  public int getTransportId() {
    return transportId;
  }

  public void setTransportId(int transportId) {
    this.transportId = transportId;
  }

  public String getGpsLocation() {
    return gpsLocation;
  }

  public void setGpsLocation(String gpsLocation) {
    this.gpsLocation = gpsLocation;
  }

  public String getTransportCode() {
    return transportCode;
  }

  public void setTransportCode(String transportCode) {
    this.transportCode = transportCode;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;

    Transport transport = (Transport) object;

    if (transportId != transport.transportId) return false;
    if (gpsLocation != null
        ? !gpsLocation.equals(transport.gpsLocation)
        : transport.gpsLocation != null) return false;
    if (transportCode != null
        ? !transportCode.equals(transport.transportCode)
        : transport.transportCode != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = transportId;
    result = 31 * result + (gpsLocation != null ? gpsLocation.hashCode() : 0);
    result = 31 * result + (transportCode != null ? transportCode.hashCode() : 0);
    return result;
  }

  public Collection<Shipment> getShipments() {
    return shipments;
  }

  public void setShipments(Collection<Shipment> shipments) {
    this.shipments = shipments;
  }
}
