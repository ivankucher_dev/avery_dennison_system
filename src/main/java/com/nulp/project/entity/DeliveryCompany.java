package com.nulp.project.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "delivery_company")
public class DeliveryCompany {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "company_id")
  private int companyId;
  private String name;
  @Column(name = "phone_number")
  private String phoneNumber;
  @ManyToOne
  @JoinColumn(name = "address")
  private Address address;
  @OneToMany(
          mappedBy = "deliveryCompany",
          cascade = CascadeType.ALL,
          orphanRemoval = true
  )
  private Collection<Shipment> shipments = new ArrayList<>();
  @OneToMany
  private Collection<ShipmentType> shipmentTypes;

  public DeliveryCompany(
      int companyId,
      String name,
      String phoneNumber,
      Address address,
      Collection<Shipment> shipments,
      Collection<ShipmentType> shipmentTypes) {
    this.companyId = companyId;
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.shipments = shipments;
    this.shipmentTypes = shipmentTypes;
  }

  public DeliveryCompany() {}

  public int getCompanyId() {
    return companyId;
  }

  public void setCompanyId(int companyId) {
    this.companyId = companyId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Collection<Shipment> getShipments() {
    return shipments;
  }

  public void setShipments(Collection<Shipment> shipments) {
    this.shipments = shipments;
  }

  public Collection<ShipmentType> getShipmentTypes() {
    return shipmentTypes;
  }

  public void setShipmentTypes(Collection<ShipmentType> shipmentTypes) {
    this.shipmentTypes = shipmentTypes;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;

    DeliveryCompany that = (DeliveryCompany) object;

    if (companyId != that.companyId) return false;
    if (name != null ? !name.equals(that.name) : that.name != null) return false;
    if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null)
      return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = companyId;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
    return result;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Collection<Shipment> getShipmentsByCompanyId() {
    return shipments;
  }

  public void setShipmentsByCompanyId(Collection<Shipment> shipments) {
    this.shipments = shipments;
  }
}
