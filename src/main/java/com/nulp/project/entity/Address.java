package com.nulp.project.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "address")
public class Address {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "address_id")
  private int addressId;

  @Column(name = "address_line_1")
  private String addressLine1;

  @Column(name = "address_line_2")
  private String addressLine2;

  @Column(name = "state")
  private String state;

  private String city;
  private int zip;

  @OneToOne
  @JoinColumn(name = "countryId")
  private Country country;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "address_id")
  private Set<Customer> customers ;

  public Address(
      String addressLine1,
      String addressLine2,
      String state,
      String city,
      int zip,
      Country country,Set<Customer> customers) {
    this.addressLine1 = addressLine1;
    this.addressLine2 = addressLine2;
    this.state = state;
    this.city = city;
    this.zip = zip;
    this.country = country;
    this.customers = customers;
  }

  public Set<Customer> getCustomers() {
    return customers;
  }

  public void setCustomers(Set<Customer> customers) {
    this.customers = customers;
  }

  public Address() {}

  public int getAddressId() {
    return addressId;
  }

  public void setAddressId(int addressId) {
    this.addressId = addressId;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public int getZip() {
    return zip;
  }

  public void setZip(int zip) {
    this.zip = zip;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;

    Address address = (Address) object;

    if (addressId != address.addressId) return false;
    if (zip != address.zip) return false;
    if (addressLine1 != null
        ? !addressLine1.equals(address.addressLine1)
        : address.addressLine1 != null) return false;
    if (addressLine2 != null
        ? !addressLine2.equals(address.addressLine2)
        : address.addressLine2 != null) return false;
    if (state != null ? !state.equals(address.state) : address.state != null) return false;
    if (city != null ? !city.equals(address.city) : address.city != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = addressId;
    result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
    result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
    result = 31 * result + (state != null ? state.hashCode() : 0);
    result = 31 * result + (city != null ? city.hashCode() : 0);
    result = 31 * result + zip;
    return result;
  }

  public Country getCountry() {
    return country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }
}
