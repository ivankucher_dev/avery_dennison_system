package com.nulp.project.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "shipment_type")
public class ShipmentType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "type_id")
    private int typeId;
    private String type;
    @OneToMany(mappedBy = "shipmentType")
    private List<Transport> transports;

    public ShipmentType(int typeId, String type) {
        this.typeId = typeId;
        this.type = type;
    }


    public ShipmentType() {
    }


    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        ShipmentType that = (ShipmentType) object;

        if (typeId != that.typeId) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = typeId;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
