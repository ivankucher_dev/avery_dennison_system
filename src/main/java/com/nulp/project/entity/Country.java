package com.nulp.project.entity;

import javax.persistence.*;

@Entity
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private int countryId;
    private String name;
    @Column(name = "global_code")
    private int globalCode;

    public Country(int countryId, String name, int globalCode) {
        this.countryId = countryId;
        this.name = name;
        this.globalCode = globalCode;
    }

    public Country(){

    }
    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGlobalCode() {
        return globalCode;
    }

    public void setGlobalCode(int globalCode) {
        this.globalCode = globalCode;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Country country = (Country) object;

        if (countryId != country.countryId) return false;
        if (globalCode != country.globalCode) return false;
        if (name != null ? !name.equals(country.name) : country.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = countryId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + globalCode;
        return result;
    }


}
