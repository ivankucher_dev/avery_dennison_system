package com.nulp.project.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "customer")
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "customer_id")
  private int customerId;

  private String name;
  private String surname;

  @Column(name = "phone_number")
  private String phoneNumber;

  private String zip;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinColumn(name = "address_id")
  private Address address_id;

  @OneToMany(mappedBy = "customer",fetch = FetchType.EAGER)
  private Set<Shipment> shipments;

  public Customer(
      int customerId,
      String name,
      String surname,
      String phoneNumber,
      String zip,
      Address address,
      Set<Shipment> shipments) {
    this.customerId = customerId;
    this.name = name;
    this.surname = surname;
    this.phoneNumber = phoneNumber;
    this.zip = zip;
    this.address_id = address;
    this.shipments = shipments;
  }

  public Customer() {}

  public int getCustomerId() {
    return customerId;
  }

  public Address getAddress_id() {
    return address_id;
  }

  public void setAddress_id(Address address_id) {
    this.address_id = address_id;
  }

  public Set<Shipment> getShipments() {
    return shipments;
  }

  public void setShipments(Set<Shipment> shipments) {
    this.shipments = shipments;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }


  public void addShipment(Shipment shipment){
    if(this.shipments==null){
      shipments = new HashSet<>();
      shipments.add(shipment);
    }
    else{
      shipments.add(shipment);
    }
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;

    Customer customer = (Customer) object;

    if (customerId != customer.customerId) return false;
    if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
    if (surname != null ? !surname.equals(customer.surname) : customer.surname != null)
      return false;
    if (phoneNumber != null
        ? !phoneNumber.equals(customer.phoneNumber)
        : customer.phoneNumber != null) return false;
    if (zip != null ? !zip.equals(customer.zip) : customer.zip != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = customerId;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
    result = 31 * result + (zip != null ? zip.hashCode() : 0);
    return result;
  }


}
