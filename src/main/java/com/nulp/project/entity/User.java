package com.nulp.project.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@ToString
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String name;
  private String surname;

  @Column(name = "phone_number")
  private String phoneNumber;

  private String login;
  private String password;

  @OneToOne
  @JoinColumn(name = "address")
  private Address address;

  @OneToOne
  @JoinColumn(name = "customer")
  private Customer customer;

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }


  @Override
  public boolean equals(Object object) {
    if (this == object) return true;
    if (object == null || getClass() != object.getClass()) return false;
    User user = (User) object;
    return id == user.id
        && Objects.equals(name, user.name)
        && Objects.equals(surname, user.surname)
        && Objects.equals(phoneNumber, user.phoneNumber)
        && Objects.equals(login, user.login)
        && Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, surname, phoneNumber, login, password);
  }
}
