<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delete user</title>
</head>
<style>
    body {
        margin: 10px;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        background-image: url(https://lh3.googleusercontent.com/Cv98KYMr5ruObn2XXQjn1ArdUleqpWfMoVGu4t-0INy38TjAezteN1xvDvlKhQkjKPTi1KTCwo_bCzUTkWfikmnWES4PcqGyqyKNg_Qb8kJCWa2KsRIW3s-yDBlMln3Xug6Wgwbifz9MtM85akkZrTDbpLUk3rjqwHsIPGFrh3KQHrNCw1J70c6W-euWUdkqHlpk43BQFoZMPsHk8KyRYtEWbhhfnuLb04y6d66aiS0apaSxCJ0MdjAm7I5tdo1qtPGpB7tw3Fgp4EUTGmHxbdDCOkKVL6nn5kZ6bAjb4HlIlsALWuEnvU9Ds2rbmQv7EJccdKMF01Ez8qGU18OCLdldlh1xo8atxW_XouuWgHJ9rN1MWtqYvSLMsmtbZzN_9nzS4zZud8v66z0NWINbPwPAY-bxCFHEThynbOaQIpmuZuAxKF8KJ5hp1axK8gN5zOR02I_0MtITGNCQqgYiZOwPsOb3pPw10581ZyZIASxxLjQjG3KS0Lak5cnLbPIoFajRnbN5j25M25bejlkl7c9WxZ4pFcE7vRiLIzC2BhJAUOClP5m2Uw8eFvCwUuwmjKNY-AOB2dGKPYhvQYw10gUWIF5Yk1JAuVvCScIOfGjPbJ_7k2jm9UruGgUgbaqrG9lJYD3V91D3GwB3pPvNn2RIyBmCvAfo9NcdO4v6_LcoF0a1JaYBdv0=w1211-h606-no) ;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        font-size: 25px;
        width: 100vw;
        height: 100vh;
        position: relative;
    }

    .styled {
        display:block;
        border: 0;
        line-height: 2.5;
        padding: 0 20px;
        font-size: 1rem;
        text-align: center;
        color: #fff;
        text-shadow: 1px 1px 1px #000;
        border-radius: 10px;
        background-color: rgb(34, 96, 220);
        background-image: linear-gradient(to top left,
        rgba(0, 0, 0, .2),
        rgba(0, 0, 0, .2) 30%,
        rgba(0, 0, 0, 0));
        box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
        inset -2px -2px 3px rgba(0, 0, 0, .6);
    }

    .styled:hover {
        background-color: rgb(147, 154, 255);
    }

    .styled:active {
        box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
        inset 2px 2px 3px rgba(0, 0, 0, .6);
    }

    .edit {
        border:1px solid #9E9E9E;
        color: #000000;
        padding: 3px;
        margin-top: 2px;
        margin-bottom: 2px;
        font-size: 11px;
        font-family: Verdana;
        background: #FFF;
    }
    h1 {
        font-size: 23px;
    }

    .bk {
        background-color: grey;
    }
    a {
        color: #92badd;
        display:inline-block;
        text-decoration: none;
        font-weight: 400;
    }

    h2 {
        text-align: center;
        font-size: 16px;
        font-weight: 600;
        text-transform: uppercase;
        display:inline-block;
        margin: 40px 8px 10px 8px;
        color: #cccccc;
    }


    /* Header/Logo Title */
    .header {
        padding: 60px;
        text-align: center;
        color: white;
        font-size: 30px;
    }

    /* STRUCTURE */

    .wrapper {
        display: flex;
        align-items: center;
        flex-direction: column;
        width: 100%;
        min-height: 100%;
        padding: 20px;
    }

    #formContent {
        -webkit-border-radius: 10px 10px 10px 10px;
        border-radius: 10px 10px 10px 10px;
        background: #fff;
        padding: 30px;
        width: 90%;
        max-width: 450px;
        position: relative;
        padding: 0px;
        -webkit-box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
        box-shadow: 0 30px 60px 0 rgba(0,0,0,0.3);
        text-align: center;
    }

    #formFooter {
        background-color: #f6f6f6;
        border-top: 1px solid #dce8f1;
        padding: 25px;
        text-align: center;
        -webkit-border-radius: 0 0 10px 10px;
        border-radius: 0 0 10px 10px;
    }



    /* TABS */

    h2.inactive {
        color: #cccccc;
    }

    h2.active {
        color: #0d0d0d;
        border-bottom: 2px solid #5fbae9;
    }



    /* FORM TYPOGRAPHY*/

    input[type=button], input[type=submit], input[type=reset]  {
        background-color: #56baed;
        border: none;
        color: white;
        padding: 15px 80px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        text-transform: uppercase;
        font-size: 13px;
        -webkit-box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
        box-shadow: 0 10px 30px 0 rgba(95,186,233,0.4);
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
        margin: 5px 20px 40px 20px;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    input[type=button]:hover, input[type=submit]:hover, input[type=reset]:hover  {
        background-color: #39ace7;
    }

    input[type=button]:active, input[type=submit]:active, input[type=reset]:active  {
        -moz-transform: scale(0.95);
        -webkit-transform: scale(0.95);
        -o-transform: scale(0.95);
        -ms-transform: scale(0.95);
        transform: scale(0.95);
    }

    input[type=text] {
        background-color: #f6f6f6;
        border: none;
        color: #0d0d0d;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 5px;
        width: 85%;
        border: 2px solid #f6f6f6;
        -webkit-transition: all 0.5s ease-in-out;
        -moz-transition: all 0.5s ease-in-out;
        -ms-transition: all 0.5s ease-in-out;
        -o-transition: all 0.5s ease-in-out;
        transition: all 0.5s ease-in-out;
        -webkit-border-radius: 5px 5px 5px 5px;
        border-radius: 5px 5px 5px 5px;
    }

    input[type=text]:focus {
        background-color: #fff;
        border-bottom: 2px solid #5fbae9;
    }

    input[type=text]:placeholder {
        color: #cccccc;
    }



    /* ANIMATIONS */

    /* Simple CSS3 Fade-in-down Animation */
    .fadeInDown {
        -webkit-animation-name: fadeInDown;
        animation-name: fadeInDown;
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }

    @-webkit-keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }

    @keyframes fadeInDown {
        0% {
            opacity: 0;
            -webkit-transform: translate3d(0, -100%, 0);
            transform: translate3d(0, -100%, 0);
        }
        100% {
            opacity: 1;
            -webkit-transform: none;
            transform: none;
        }
    }

    /* Simple CSS3 Fade-in Animation */
    @-webkit-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
    @-moz-keyframes fadeIn { from { opacity:0; } to { opacity:1; } }
    @keyframes fadeIn { from { opacity:0; } to { opacity:1; } }

    .fadeIn {
        opacity:0;
        -webkit-animation:fadeIn ease-in 1;
        -moz-animation:fadeIn ease-in 1;
        animation:fadeIn ease-in 1;

        -webkit-animation-fill-mode:forwards;
        -moz-animation-fill-mode:forwards;
        animation-fill-mode:forwards;

        -webkit-animation-duration:1s;
        -moz-animation-duration:1s;
        animation-duration:1s;
    }

    .fadeIn.first {
        -webkit-animation-delay: 0.4s;
        -moz-animation-delay: 0.4s;
        animation-delay: 0.4s;
    }

    .fadeIn.second {
        -webkit-animation-delay: 0.6s;
        -moz-animation-delay: 0.6s;
        animation-delay: 0.6s;
    }

    .fadeIn.third {
        -webkit-animation-delay: 0.8s;
        -moz-animation-delay: 0.8s;
        animation-delay: 0.8s;
    }

    .fadeIn.fourth {
        -webkit-animation-delay: 1s;
        -moz-animation-delay: 1s;
        animation-delay: 1s;
    }

    /* Simple CSS3 Fade-in Animation */
    .underlineHover:after {
        display: block;
        left: 0;
        bottom: -10px;
        width: 0;
        height: 2px;
        background-color: #56baed;
        content: "";
        transition: width 0.2s;
    }

    .underlineHover:hover {
        color: #0d0d0d;
    }

    .underlineHover:hover:after{
        width: 100%;
    }



    /* OTHERS */

    *:focus {
        outline: none;
    }

    #icon {
        width:60%;
    }

    * {
        box-sizing: border-box;
    }
</style>

<body>
<div class="header">
    <img src="https://lh3.googleusercontent.com/gi7lma_c5VdDw_UuR68670a1jgb_Kr9JKpx8r7T80Z6U9CT2zZ0hqmKaZxPxsyHs3Qed8FUhgERVstnACkaJqOQHd47WDYH1Ey2GqZhEMtBBaFkNVI2OmWPxxF1zPS8MWIjGZ26ME_kFQCRJqLU9uyfGI3fiacNdLkBMVtsIrWfrhOaAJorvVyYzRRkUGeUBj1KhhRMtsAVw9aKSoFkQVKLRKMmxnNby8S9IWL1v_VD4vkiAYlZ6ZA7WLa6keM2i7ywevh0525kDwdJpzmqzl3qBmMCYj0jCuDdwP5SVPofB87GAqKr2QInCFRUhs21fbZG2GRvBaqDCVhL6T3Qitj_JN7FUAXDYTJvdxH3nAEoIT4eV0H2R8oVdsKSskMsHV5W4SikbRLmawAUqnX3E5fzfPCBXxvljd1lmA64jWVg08LZSlNYeqRbZpI-SQ8QcV4TSSKpNZiZihMaNzcYDflSimMeBBuFOkn46iWQov0Z887slaM8UipmhSvhBHx65eKE_CmJe1--nxKr01gr31XCAGPDmBUtxflIK0xaHNp1r2Hb_Cn38BkXkEOVVw6d-Rge5NDMOiWyGjTGGZd9YvkOIjiwUwGAKL5YC-QvaZOH21mJcNqc-1dncTYRUOcGhZxsjXMtstAhUop4h8ioYJs9Ho-XgZwFmrCtR-tOplqONj5HFpvikWVQ=w189-h424-no" >

</div>

<form action="/admin/users/delete-user" method="get">
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->
        <h2 class="active">Enter user id</h2>
        <!-- Login Form -->
            <input type="text" id="login" class="fadeIn second" name="userId" placeholder="user id" required>
            <input type="submit" class="fadeIn fourth" value="Show">

    </div>
</div>
</form>
</body>

</html>