<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Successfull</title>
</head>
<style>
    body {
        margin: 10px;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        background-image: url(https://lh3.googleusercontent.com/Cv98KYMr5ruObn2XXQjn1ArdUleqpWfMoVGu4t-0INy38TjAezteN1xvDvlKhQkjKPTi1KTCwo_bCzUTkWfikmnWES4PcqGyqyKNg_Qb8kJCWa2KsRIW3s-yDBlMln3Xug6Wgwbifz9MtM85akkZrTDbpLUk3rjqwHsIPGFrh3KQHrNCw1J70c6W-euWUdkqHlpk43BQFoZMPsHk8KyRYtEWbhhfnuLb04y6d66aiS0apaSxCJ0MdjAm7I5tdo1qtPGpB7tw3Fgp4EUTGmHxbdDCOkKVL6nn5kZ6bAjb4HlIlsALWuEnvU9Ds2rbmQv7EJccdKMF01Ez8qGU18OCLdldlh1xo8atxW_XouuWgHJ9rN1MWtqYvSLMsmtbZzN_9nzS4zZud8v66z0NWINbPwPAY-bxCFHEThynbOaQIpmuZuAxKF8KJ5hp1axK8gN5zOR02I_0MtITGNCQqgYiZOwPsOb3pPw10581ZyZIASxxLjQjG3KS0Lak5cnLbPIoFajRnbN5j25M25bejlkl7c9WxZ4pFcE7vRiLIzC2BhJAUOClP5m2Uw8eFvCwUuwmjKNY-AOB2dGKPYhvQYw10gUWIF5Yk1JAuVvCScIOfGjPbJ_7k2jm9UruGgUgbaqrG9lJYD3V91D3GwB3pPvNn2RIyBmCvAfo9NcdO4v6_LcoF0a1JaYBdv0=w1211-h606-no);
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        font-size: 25px;
        width: 100vw;
        height: 100vh;
        position: relative;
    }
    form {
        /* position: absolute; */
        padding: 15px;
        width: max-content;
        margin: 100px auto;
        background-color: aliceblue;
        border-radius: 10px;
    }
    .styled {
        display:block;
        border: 0;
        line-height: 2.5;
        padding: 0 20px;
        font-size: 1rem;
        text-align: center;
        color: #fff;
        text-shadow: 1px 1px 1px #000;
        border-radius: 10px;
        background-color: rgb(34, 96, 220);
        background-image: linear-gradient(to top left,
        rgba(0, 0, 0, .2),
        rgba(0, 0, 0, .2) 30%,
        rgba(0, 0, 0, 0));
        box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
        inset -2px -2px 3px rgba(0, 0, 0, .6);
    }

    .styled:hover {
        background-color: rgb(147, 154, 255);
    }

    .styled:active {
        box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
        inset 2px 2px 3px rgba(0, 0, 0, .6);
    }
    h2.active {
        color: black;
        border-bottom: 2px solid red;
    }
</style>

<body>
<!-- START LOGO -->
<tr>
    <td width="100%" valign="top" align="center" class="padding-container" style="padding: 18px 0px 18px 0px!important; mso-padding-alt: 18px 0px 18px 0px;">
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="wrapper">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="100%" valign="top" align="center">
                                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="wrapper" >
                                    <tr>
                                        <td align="center">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="container" align="center">
                                                <!-- START HEADER IMAGE -->
                                                <tr>
                                                    <td align="center" class="hund" width="600">
                                                        <img src="https://lh3.googleusercontent.com/gi7lma_c5VdDw_UuR68670a1jgb_Kr9JKpx8r7T80Z6U9CT2zZ0hqmKaZxPxsyHs3Qed8FUhgERVstnACkaJqOQHd47WDYH1Ey2GqZhEMtBBaFkNVI2OmWPxxF1zPS8MWIjGZ26ME_kFQCRJqLU9uyfGI3fiacNdLkBMVtsIrWfrhOaAJorvVyYzRRkUGeUBj1KhhRMtsAVw9aKSoFkQVKLRKMmxnNby8S9IWL1v_VD4vkiAYlZ6ZA7WLa6keM2i7ywevh0525kDwdJpzmqzl3qBmMCYj0jCuDdwP5SVPofB87GAqKr2QInCFRUhs21fbZG2GRvBaqDCVhL6T3Qitj_JN7FUAXDYTJvdxH3nAEoIT4eV0H2R8oVdsKSskMsHV5W4SikbRLmawAUqnX3E5fzfPCBXxvljd1lmA64jWVg08LZSlNYeqRbZpI-SQ8QcV4TSSKpNZiZihMaNzcYDflSimMeBBuFOkn46iWQov0Z887slaM8UipmhSvhBHx65eKE_CmJe1--nxKr01gr31XCAGPDmBUtxflIK0xaHNp1r2Hb_Cn38BkXkEOVVw6d-Rge5NDMOiWyGjTGGZd9YvkOIjiwUwGAKL5YC-QvaZOH21mJcNqc-1dncTYRUOcGhZxsjXMtstAhUop4h8ioYJs9Ho-XgZwFmrCtR-tOplqONj5HFpvikWVQ=w189-h424-no"  </td>
                                                </tr>
                                                <!-- END HEADER IMAGE -->
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>

<form action="" method="get">
    <h2 class="active"> ${message} </h2>
    <input class="favorite styled" type="submit" value="Go to main page" formaction="/profile" style=" width: 250px; margin: 0 auto;">
</form>
</body>

</html>