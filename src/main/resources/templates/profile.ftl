<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<style>
    body {
        margin: 10px;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        background-image: url(https://lh3.googleusercontent.com/Cv98KYMr5ruObn2XXQjn1ArdUleqpWfMoVGu4t-0INy38TjAezteN1xvDvlKhQkjKPTi1KTCwo_bCzUTkWfikmnWES4PcqGyqyKNg_Qb8kJCWa2KsRIW3s-yDBlMln3Xug6Wgwbifz9MtM85akkZrTDbpLUk3rjqwHsIPGFrh3KQHrNCw1J70c6W-euWUdkqHlpk43BQFoZMPsHk8KyRYtEWbhhfnuLb04y6d66aiS0apaSxCJ0MdjAm7I5tdo1qtPGpB7tw3Fgp4EUTGmHxbdDCOkKVL6nn5kZ6bAjb4HlIlsALWuEnvU9Ds2rbmQv7EJccdKMF01Ez8qGU18OCLdldlh1xo8atxW_XouuWgHJ9rN1MWtqYvSLMsmtbZzN_9nzS4zZud8v66z0NWINbPwPAY-bxCFHEThynbOaQIpmuZuAxKF8KJ5hp1axK8gN5zOR02I_0MtITGNCQqgYiZOwPsOb3pPw10581ZyZIASxxLjQjG3KS0Lak5cnLbPIoFajRnbN5j25M25bejlkl7c9WxZ4pFcE7vRiLIzC2BhJAUOClP5m2Uw8eFvCwUuwmjKNY-AOB2dGKPYhvQYw10gUWIF5Yk1JAuVvCScIOfGjPbJ_7k2jm9UruGgUgbaqrG9lJYD3V91D3GwB3pPvNn2RIyBmCvAfo9NcdO4v6_LcoF0a1JaYBdv0=w1211-h606-no);
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        font-size: 25px;
        width: 100vw;
        height: 100vh;
        position: relative;
    }

    .profile {
        margin: 20px 0;
    }

    /* Profile sidebar */
    .profile-sidebar {
        padding: 20px 0 10px 0;
        background: #fff;
    }

    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .profile-usertitle {
        text-align: center;
        margin-top: 20px;
    }

    .profile-usertitle-name {
        color: #5a7391;
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 7px;
    }

    .profile-usertitle-job {
        text-transform: uppercase;
        color: #5b9bd1;
        font-size: 12px;
        font-weight: 600;
        margin-bottom: 15px;
    }

    .profile-userbuttons {
        text-align: center;
        margin-top: 10px;
    }

    .profile-userbuttons .btn {
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 600;
        padding: 6px 15px;
        margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
        margin-right: 0px;
    }

    .profile-usermenu {
        margin-top: 30px;
    }

    .profile-usermenu ul li {
        border-bottom: 1px solid #f0f4f7;
    }

    .profile-usermenu ul li:last-child {
        border-bottom: none;
    }

    .profile-usermenu ul li a {
        color: #93a3b5;
        font-size: 14px;
        font-weight: 400;
    }

    .profile-usermenu ul li a i {
        margin-right: 8px;
        font-size: 14px;
    }

    .profile-usermenu ul li a:hover {
        background-color: #fafcfd;
        color: #5b9bd1;
    }

    .profile-usermenu ul li.active {
        border-bottom: none;
    }

    .profile-usermenu ul li.active a {
        color: #5b9bd1;
        background-color: #f6f9fb;
        border-left: 2px solid #5b9bd1;
        margin-left: -2px;
    }

    /* Profile Content */
    .profile-content {
        padding: 20px;
        background: #fff;
        min-height: 460px;
    }
    .styled {
        display:block;
        border: 0;
        line-height: 2.5;
        padding: 0 20px;
        font-size: 1rem;
        text-align: center;
        color: #fff;
        text-shadow: 1px 1px 1px #000;
        border-radius: 10px;
        background-color: rgb(34, 96, 220);
        background-image: linear-gradient(to top left,
        rgba(0, 0, 0, .2),
        rgba(0, 0, 0, .2) 30%,
        rgba(0, 0, 0, 0));
        box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
        inset -2px -2px 3px rgba(0, 0, 0, .6);
    }

    .styled:hover {
        background-color: rgb(147, 154, 255);
    }

    .styled:active {
        box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
        inset 2px 2px 3px rgba(0, 0, 0, .6);
    }
    .profile-content p {
        text-align: center;
        font-family: "Arial Black", Gadget, sans-serif;
        font-size: 30px;
        letter-spacing: 2px;
        word-spacing: 2px;
        color: #08237A;
        font-weight: 700;
        text-decoration: overline solid rgb(68, 68, 68);
        font-style: normal;
        font-variant: small-caps;
        text-transform: none;
    }
</style>

<body>
<!-- START LOGO -->
<tr>
    <td width="100%" valign="top" align="center" class="padding-container" style="padding: 18px 0px 18px 0px!important; mso-padding-alt: 18px 0px 18px 0px;">
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="wrapper">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="100%" valign="top" align="center">
                                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="wrapper" >
                                    <tr>
                                        <td align="center">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="container" align="center">
                                                <!-- START HEADER IMAGE -->
                                                <tr>
                                                    <td align="center" class="hund" width="600">
                                                        <img src="https://lh3.googleusercontent.com/gi7lma_c5VdDw_UuR68670a1jgb_Kr9JKpx8r7T80Z6U9CT2zZ0hqmKaZxPxsyHs3Qed8FUhgERVstnACkaJqOQHd47WDYH1Ey2GqZhEMtBBaFkNVI2OmWPxxF1zPS8MWIjGZ26ME_kFQCRJqLU9uyfGI3fiacNdLkBMVtsIrWfrhOaAJorvVyYzRRkUGeUBj1KhhRMtsAVw9aKSoFkQVKLRKMmxnNby8S9IWL1v_VD4vkiAYlZ6ZA7WLa6keM2i7ywevh0525kDwdJpzmqzl3qBmMCYj0jCuDdwP5SVPofB87GAqKr2QInCFRUhs21fbZG2GRvBaqDCVhL6T3Qitj_JN7FUAXDYTJvdxH3nAEoIT4eV0H2R8oVdsKSskMsHV5W4SikbRLmawAUqnX3E5fzfPCBXxvljd1lmA64jWVg08LZSlNYeqRbZpI-SQ8QcV4TSSKpNZiZihMaNzcYDflSimMeBBuFOkn46iWQov0Z887slaM8UipmhSvhBHx65eKE_CmJe1--nxKr01gr31XCAGPDmBUtxflIK0xaHNp1r2Hb_Cn38BkXkEOVVw6d-Rge5NDMOiWyGjTGGZd9YvkOIjiwUwGAKL5YC-QvaZOH21mJcNqc-1dncTYRUOcGhZxsjXMtstAhUop4h8ioYJs9Ho-XgZwFmrCtR-tOplqONj5HFpvikWVQ=w189-h424-no"  </td>
                                                </tr>
                                                <!-- END HEADER IMAGE -->
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>

<form action="/profile/registerCustomer" method="post" id="registerCustomer">
    <input type="hidden" name="login" value="${user.login}">
    <input type="hidden" name="password" value="${user.password}">
</form>

<form action="/profile/customerOverview" method="get" id="customerOverview">
    <input type="hidden" name="login" value="${user.login}">
    <input type="hidden" name="password" value="${user.password}">
</form>

<div class="container">
    <div class="row profile">
        <div class="col-md-3">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="http://keenthemes.com/preview/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        ${user.name}&nbsp;${user.surname}
                    </div>
                    <div class="profile-usertitle-job">
                        Phone number : ${user.phoneNumber}
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <#if (user.customer)??>
                    <div class="profile-userbuttons">
                        <button type="submit" form="customerOverview" class="btn btn-success btn-sm">You are already customer</button>
                    </div>
                <#else>
                <div class="profile-userbuttons">
                    <button type="submit" form="registerCustomer" class="btn btn-danger btn-sm">Start being customer</button>
                </div>
                </#if>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="/profile/userOverview">
                                <i class="glyphicon glyphicon-home"></i>
                                Overview </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="glyphicon glyphicon-user"></i>
                                Account Settings </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <i class="glyphicon glyphicon-ok"></i>
                                Tasks </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="glyphicon glyphicon-flag"></i>
                                Help </a>
                        </li>
                    </ul>
                </div>
                <!-- END MENU -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="profile-content">
                <p>Status panel</p>
                <#if (statusMessage)??>
                    ${statusMessage}
                <#else>
                  Some user related data goes here. Status of changes will be here....
                </#if>
            </div>
        </div>
    </div>
</div>
<br>
<br>
</body>

</html>