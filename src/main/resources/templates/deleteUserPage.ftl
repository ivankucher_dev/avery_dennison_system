<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delete user</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<style>
    body {
        margin: 10px;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
        background-image: url(https://lh3.googleusercontent.com/Cv98KYMr5ruObn2XXQjn1ArdUleqpWfMoVGu4t-0INy38TjAezteN1xvDvlKhQkjKPTi1KTCwo_bCzUTkWfikmnWES4PcqGyqyKNg_Qb8kJCWa2KsRIW3s-yDBlMln3Xug6Wgwbifz9MtM85akkZrTDbpLUk3rjqwHsIPGFrh3KQHrNCw1J70c6W-euWUdkqHlpk43BQFoZMPsHk8KyRYtEWbhhfnuLb04y6d66aiS0apaSxCJ0MdjAm7I5tdo1qtPGpB7tw3Fgp4EUTGmHxbdDCOkKVL6nn5kZ6bAjb4HlIlsALWuEnvU9Ds2rbmQv7EJccdKMF01Ez8qGU18OCLdldlh1xo8atxW_XouuWgHJ9rN1MWtqYvSLMsmtbZzN_9nzS4zZud8v66z0NWINbPwPAY-bxCFHEThynbOaQIpmuZuAxKF8KJ5hp1axK8gN5zOR02I_0MtITGNCQqgYiZOwPsOb3pPw10581ZyZIASxxLjQjG3KS0Lak5cnLbPIoFajRnbN5j25M25bejlkl7c9WxZ4pFcE7vRiLIzC2BhJAUOClP5m2Uw8eFvCwUuwmjKNY-AOB2dGKPYhvQYw10gUWIF5Yk1JAuVvCScIOfGjPbJ_7k2jm9UruGgUgbaqrG9lJYD3V91D3GwB3pPvNn2RIyBmCvAfo9NcdO4v6_LcoF0a1JaYBdv0=w1211-h606-no);
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        font-size: 25px;
        width: 100vw;
        height: 100vh;
        position: relative;
    }

    .profile {
        position:fixed;
        top: 50%;
        left: 50%;
        margin-left: -15em; /*set to a negative number 1/2 of your width*/
    }

    /* Profile sidebar */
    .profile-sidebar {
        padding: 20px 0 10px 0;
        background: #fff;
    }

    form {
        /* position: absolute; */
        padding: 15px;
        width: max-content;
        background-color: aliceblue;
        border-radius: 10px;
    }
    .styled {
        display:block;
        border: 0;
        line-height: 10;
        padding: 0 20px;
        font-size: 10rem;
        text-align: center;
        color: #fff;
        text-shadow: 1px 1px 1px #000;
        border-radius: 10px;
        background-color: rgba(220, 0, 0, 1);
        background-image: linear-gradient(to top left,
        rgba(0, 0, 0, .2),
        rgba(0, 0, 0, .2) 30%,
        rgba(0, 0, 0, 0));
        box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
        inset -2px -2px 3px rgba(0, 0, 0, .6);
    }

    .styled:hover {
        background-color: rgba(255, 0, 0, 1);
    }

    .styled:active {
        box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
        inset 2px 2px 3px rgba(0, 0, 0, .6);
    }

    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        height: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .profile-usertitle {
        text-align: center;
        margin-top: 20px;
    }

    .profile-usertitle-name {
        color: #5a7391;
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 7px;
    }

    .profile-usertitle-job {
        text-transform: uppercase;
        color: #5b9bd1;
        font-size: 12px;
        font-weight: 600;
        margin-bottom: 15px;
    }

    .profile-userbuttons {
        text-align: center;
        margin-top: 10px;
    }

    .profile-userbuttons .btn {
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 600;
        padding: 6px 15px;
        margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
        margin-right: 0px;
    }

    .profile-usermenu {
        margin-top: 30px;
    }

    .profile-usermenu ul li {
        border-bottom: 1px solid #f0f4f7;
    }

    .profile-usermenu ul li:last-child {
        border-bottom: none;
    }

    .profile-usermenu ul li a {
        color: #93a3b5;
        font-size: 14px;
        font-weight: 400;
    }

    .profile-usermenu ul li a i {
        margin-right: 8px;
        font-size: 14px;
    }

    .profile-usermenu ul li a:hover {
        background-color: #fafcfd;
        color: #5b9bd1;
    }

    .profile-usermenu ul li.active {
        border-bottom: none;
    }

    .profile-usermenu ul li.active a {
        color: #5b9bd1;
        background-color: #f6f9fb;
        border-left: 2px solid #5b9bd1;
        margin-left: -2px;
    }

    /* Profile Content */
    .profile-content {
        padding: 20px;
        background: #fff;
        min-height: 400px;
    }
    .styled {
        display:block;
        border: 0;
        line-height: 2.5;
        padding: 0 20px;
        font-size: 1rem;
        text-align: center;
        color: #fff;
        text-shadow: 1px 1px 1px #000;
        border-radius: 10px;
        background-color: rgb(34, 96, 220);
        background-image: linear-gradient(to top left,
        rgba(0, 0, 0, .2),
        rgba(0, 0, 0, .2) 30%,
        rgba(0, 0, 0, 0));
        box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
        inset -2px -2px 3px rgba(0, 0, 0, .6);
    }

    .styled:hover {
        background-color: rgb(147, 154, 255);
    }

    .styled:active {
        box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
        inset 2px 2px 3px rgba(0, 0, 0, .6);
    }

    .admin-usertitle {
        text-align: center;
        margin-top: 20px;
        color: darkgreen;
    }

    .select-css {
        display: block;
        font-size: 16px;
        font-family: sans-serif;
        font-weight: 700;
        color: #444;
        line-height: 1.3;
        padding: .6em 1.4em .5em .8em;
        width: 100%;
        max-width: 100%;
        box-sizing: border-box;
        margin: 0;
        border: 1px solid #aaa;
        box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
        border-radius: .5em;
        -moz-appearance: none;
        -webkit-appearance: none;
        appearance: none;
        background-color: #fff;
        background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E'),
        linear-gradient(to bottom, #ffffff 0%,#e5e5e5 100%);
        background-repeat: no-repeat, repeat;
        background-position: right .7em top 50%, 0 0;
        background-size: .65em auto, 100%;
    }
    .select-css::-ms-expand {
        display: none;
    }
    .select-css:hover {
        border-color: #888;
    }
    .select-css:focus {
        border-color: #aaa;
        box-shadow: 0 0 1px 3px rgba(59, 153, 252, .7);
        box-shadow: 0 0 0 3px -moz-mac-focusring;
        color: #222;
        outline: none;
    }
    .select-css option {
        font-weight:normal;
    }
</style>

<body>
<!-- START LOGO -->
<tr>
    <td width="100%" valign="top" align="center" class="padding-container" style="padding: 18px 0px 18px 0px!important; mso-padding-alt: 18px 0px 18px 0px;">
        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="wrapper">
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="100%" valign="top" align="center">
                                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="wrapper" >
                                    <tr>
                                        <td align="center">
                                            <table width="600" cellpadding="0" cellspacing="0" border="0" class="container" align="center">
                                                <!-- START HEADER IMAGE -->
                                                <tr>
                                                    <td align="center" class="hund" width="600">
                                                        <img src="https://lh3.googleusercontent.com/gi7lma_c5VdDw_UuR68670a1jgb_Kr9JKpx8r7T80Z6U9CT2zZ0hqmKaZxPxsyHs3Qed8FUhgERVstnACkaJqOQHd47WDYH1Ey2GqZhEMtBBaFkNVI2OmWPxxF1zPS8MWIjGZ26ME_kFQCRJqLU9uyfGI3fiacNdLkBMVtsIrWfrhOaAJorvVyYzRRkUGeUBj1KhhRMtsAVw9aKSoFkQVKLRKMmxnNby8S9IWL1v_VD4vkiAYlZ6ZA7WLa6keM2i7ywevh0525kDwdJpzmqzl3qBmMCYj0jCuDdwP5SVPofB87GAqKr2QInCFRUhs21fbZG2GRvBaqDCVhL6T3Qitj_JN7FUAXDYTJvdxH3nAEoIT4eV0H2R8oVdsKSskMsHV5W4SikbRLmawAUqnX3E5fzfPCBXxvljd1lmA64jWVg08LZSlNYeqRbZpI-SQ8QcV4TSSKpNZiZihMaNzcYDflSimMeBBuFOkn46iWQov0Z887slaM8UipmhSvhBHx65eKE_CmJe1--nxKr01gr31XCAGPDmBUtxflIK0xaHNp1r2Hb_Cn38BkXkEOVVw6d-Rge5NDMOiWyGjTGGZd9YvkOIjiwUwGAKL5YC-QvaZOH21mJcNqc-1dncTYRUOcGhZxsjXMtstAhUop4h8ioYJs9Ho-XgZwFmrCtR-tOplqONj5HFpvikWVQ=w189-h424-no"  </td>
                                                </tr>
                                                <!-- END HEADER IMAGE -->
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>

<div class="container">
    <div class="row profile">
        <div class="col-md-9">
            <div class="profile-content">

                <font size="2" face="Courier New" >
                    <table id="user" border="1" width="100%">
                        <tr>
                            <th>User name</th>
                            <th>User Surname</th>
                            <th>Phone number</th>
                            <th>Address line 1</th>
                            <th>Address line 2</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Zip</th>
                            <th>Country</th>
                        </tr>
                        <tr>
                            <td>${user.name}</td>
                            <td>${user.surname}</td>
                            <td>${user.phoneNumber}</td>
                            <td>${user.address.addressLine1}</td>
                            <td>${user.address.addressLine2}</td>
                            <td>${user.address.state}</td>
                            <td>${user.address.city}</td>
                            <td>${user.address.zip}</td>
                            <td>${user.address.country.name}</td>
                        </tr>
                    </table>
                </font>
                <#if user.customer??>
                    <div class="customer-usertitle">
                        Customer information
                    </div>
                    <font size="2" face="Courier New" >
                        <table id="customer" border="1" width="100%">
                            <tr>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Zip</th>
                                <th>Phone Number</th>
                            </tr>
                            <tr>
                                <td>${user.customer.name}</td>
                                <td>${user.customer.surname}</td>
                                <td>${user.customer.zip}</td>
                                <td>${user.customer.phoneNumber}</td>
                            </tr>
                        </table>
                    </font>
                <#else>
                    <div class="customer-usertitle">
                        You dont have registered  [CUSTOMER] status
                    </div>
                </#if>

                <form action="/admin/users/delete-user" method="post">
                    <input type="hidden" id="userId"  name="userId" value="${user.id}">
                    <br><br>
                    <input class="favorite styled" type="submit" name="submit-pack-btn" value="Delete user and customer" style=" width: 250px; margin: 0 auto;">
                </form>

            </div>
        </div>
    </div>
</div>
<br>
<br>
</body>

</html>